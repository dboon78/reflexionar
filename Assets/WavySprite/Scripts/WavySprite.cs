﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshFilter)),RequireComponent(typeof(MeshRenderer))]
[ExecuteInEditMode]
public class WavySprite:MonoBehaviour{

	[HideInInspector]
	public  MeshRenderer mr;
	private Material mat;
	private MeshFilter mf;
	private Mesh mesh;
	private List<Vector3> vertices=new List<Vector3>(200);
	private List<Vector3> uvs=new List<Vector3>(200);
	private List<Color> colors=new List<Color>(200);
	private int[] triangles;
	private int trianglesCount;

	public Texture2D texture;
	private Texture2D _texture;

	public Color tint=Color.white;
	private Color _tint;

	public bool lit=false;
	private bool _lit;

	[Range(0,100)]
	public int divisionsX=30;
	private int _divisionsX;

	[Range(0,100)]
	public int divisionsY=30;
	private int _divisionsY;

	public enum waveDirections{Vertical,Horizontal};
	public waveDirections waveDirection=waveDirections.Vertical;
	private waveDirections? _waveDirection=null;

	public enum objSides{Top,Right,Bottom,Left,TopBottom,LeftRight,None}
	public objSides staticSide=objSides.Bottom;
	private objSides? _staticSide=null;

	[Range(-100,100)]
	public float waveFrequency=10f;
	private float _waveFrequency;

	[Range(0f,10f)]
	public float waveForce=0.03f;
	private float _waveForce;

	[Range(0f,100f)]
	public float waveSpeed=1f;
	private float _waveSpeed;

	float meshWidth=1;
	float meshHeight=1;

	[HideInInspector]
	public int sortingLayer=0;
	private int _sortingLayer;

	[HideInInspector]
	public int orderInLayer=0;
	private int _orderInLayer=0;

	private void Awake(){
		mr=GetComponent<MeshRenderer>();
		mf=GetComponent<MeshFilter>();
		mf.sharedMesh=null; //Reset shared mesh and material 
		mr.sharedMaterial=null; //in case the object was duplicated
		_waveDirection=null;
		_staticSide=null;
		SetMeshAndMaterial();
		GenerateMesh();
		sortingLayer=mr.sortingLayerID;
		orderInLayer=mr.sortingOrder;
		Update();
	}

	void Update(){
		if(
			_texture!=texture || 
			_tint!=tint ||
			_lit!=lit ||
			_divisionsX!=divisionsX || 
			_divisionsY!=divisionsY || 
			_waveDirection!=waveDirection || 
			_staticSide!=staticSide || 
			_waveFrequency!=waveFrequency || 
			_waveForce!=waveForce ||
			_waveSpeed!=waveSpeed ||
			_sortingLayer!=sortingLayer ||
			_orderInLayer!=orderInLayer
		){
			if(_lit!=lit){ 
				DestroyImmediate(mat);
				_lit=lit;
			}
			SetMeshAndMaterial();
			_divisionsX=divisionsX;
			_divisionsY=divisionsY;
			if(_waveDirection!=waveDirection){
				mat.SetFloat("_WaveDirection",waveDirection==waveDirections.Vertical?0:1);
				_waveDirection=waveDirection;
			}
			if(_staticSide!=staticSide){
				if(staticSide==objSides.Top) mat.SetFloat("_StaticSide",1);
				if(staticSide==objSides.Right) mat.SetFloat("_StaticSide",2);
				if(staticSide==objSides.Bottom) mat.SetFloat("_StaticSide",3);
				if(staticSide==objSides.Left) mat.SetFloat("_StaticSide",4);
				if(staticSide==objSides.TopBottom) mat.SetFloat("_StaticSide",5);
				if(staticSide==objSides.LeftRight) mat.SetFloat("_StaticSide",6);
				if(staticSide==objSides.None) mat.SetFloat("_StaticSide",0);
				_staticSide=staticSide;
			}
			_waveFrequency=waveFrequency;
			_waveForce=waveForce;
			_waveSpeed=waveSpeed;
			mat.SetFloat("_WaveFrequency",waveFrequency);
			mat.SetFloat("_WaveForce",waveForce);
			mat.SetFloat("_WaveSpeed",waveSpeed);
			if(_texture!=texture){
				_texture=texture;
				mat.SetTexture("_MainTex",texture);
				if(texture!=null){
					if(texture.width>texture.height){
						meshWidth=1f;
						meshHeight=(float)texture.height/(float)texture.width;
					}else{
						meshWidth=(float)texture.width/(float)texture.height;
						meshHeight=1f;
					}
				}else{
					meshWidth=1f;
					meshHeight=1f;
				}
			}
			_tint=tint;
			mat.SetColor("_Color",tint);
			if(_sortingLayer!=sortingLayer || _orderInLayer!=orderInLayer){
				mr.sortingLayerID=sortingLayer;
				mr.sortingOrder=orderInLayer;
				_sortingLayer=sortingLayer;
				_orderInLayer=orderInLayer;
			}
			GenerateMesh();
		}
	}

	void SetMeshAndMaterial(){
		if(mesh==null){
			mesh=new Mesh();
			mesh.name="WavySpriteMesh";
			if(mf.sharedMesh!=null) DestroyImmediate(mf.sharedMesh);
		}
		if(mf.sharedMesh==null){
			mf.sharedMesh=mesh;
		}
		if(mat==null){
			if(!lit){
				mat=new Material(Shader.Find("Custom/WavySpriteUnlit"));
				mat.name="WavySpriteUnlitMaterial";
			}else{ 
				mat=new Material(Shader.Find("Custom/WavySpriteLit"));
				mat.name="WavySpriteLitMaterial";
			}
			mat.SetTexture("_MainTex",texture);
			if(mr.sharedMaterial!=null) DestroyImmediate(mr.sharedMaterial);
		}
		if(mr.sharedMaterial==null){
			mr.sharedMaterial=mat;
		}
	}
	
	void GenerateMesh(){
		int pointsX=divisionsX+2;
		int pointsY=divisionsY+2;
		int verticeNum=0;
		int squareNum=-1;
		vertices.Clear();
		uvs.Clear();
		colors.Clear();
		triangles=new int[(((divisionsX+1)*(divisionsY+1))*2)*3];
		for(int y=0;y<pointsY;y++){
			for(int x=0;x<pointsX;x++){
				vertices.Add(new Vector3(
					(((float)x/(float)(pointsX-1))-0.5f)*meshWidth,
					((float)y/(float)(pointsY-1))*meshHeight,
					0f
				));
				uvs.Add(new Vector3(
					((float)x/(float)(pointsX-1)),
					((float)y/(float)(pointsY-1)),
					0f
				));
				if(x>0 && y>0){
					verticeNum=x+(y*pointsX);
					squareNum++;
					triangles[squareNum*6]=verticeNum-pointsX-1;
					triangles[squareNum*6+1]=verticeNum-1;
					triangles[squareNum*6+2]=verticeNum;
					triangles[squareNum*6+3]=verticeNum;
					triangles[squareNum*6+4]=verticeNum-pointsX;
					triangles[squareNum*6+5]=verticeNum-pointsX-1;
				}
			}
		}
		mesh.Clear();
		mesh.SetVertices(vertices);
		mesh.SetColors(colors);
		mesh.SetUVs(0,uvs);
		mesh.RecalculateBounds();
		mesh.RecalculateNormals();
		mesh.SetTriangles(triangles,0);
		trianglesCount=triangles.Length/3;
	}

	public int triangleCount{
		get{return trianglesCount;}
	}

}
