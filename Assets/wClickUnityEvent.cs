﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
public class wClickUnityEvent : MonoBehaviour
{

    public UnityEvent ClickEvent;
    private void OnMouseDown()
    {
        if (ClickEvent != null)
        {
            ClickEvent.Invoke();
        }
    }
}
