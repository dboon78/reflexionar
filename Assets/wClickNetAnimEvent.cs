﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class wClickNetAnimEvent : MonoBehaviour
{
    public string objectName;
    public string animationName;
    private float lastClick = 0;
    private void OnMouseUp()
    {
        if (lastClick == 0 || Time.time >  lastClick+10f)
        {
            
            lastClick = Time.time;
            wNetworkMessage message = new wNetworkMessage()
            {
                netFish = null,
                animEvent = new wAnimationEvent()
                {
                    ObjectName = objectName,
                    AnimationName = animationName
                }
            };

            string jsonstr = JsonUtility.ToJson(message);
            wNetworkManager.instance.SendNetworkMessage(jsonstr);
        }
    }
}
