﻿
/***********************************************************************************************************
 * Produced by App Advisory - http://app-advisory.com													   *
 * Facebook: https://facebook.com/appadvisory															   *
 * Contact us: https://appadvisory.zendesk.com/hc/en-us/requests/new									   *
 * App Advisory Unity Asset Store catalog: http://u3d.as/9cs											   *
 * Developed by Gilbert Anthony Barouch - https://www.linkedin.com/in/ganbarouch                           *
 ***********************************************************************************************************/




using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using AppAdvisory.VSGIF;
using System.IO;
namespace AppAdvisory.VSGIF.Exemple
{
	public class BtnShare : MonoBehaviour 
	{
		public ShareType shareType;

		public void OnClicked()
		{
			print("BtnShare - OnClicked");


			Record.DOShare(shareType);
		}
        
    }
}