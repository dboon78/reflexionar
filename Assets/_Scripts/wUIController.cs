﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AppAdvisory.VSGIF;
using UnityEngine.UI;
public class wUIController : MonoBehaviour
{
    private bool recordState = false,
                 shareState = false;
    //[SerializeField]
    //private Image recordButtonImage;
    [SerializeField]
    private GameObject gifProcessing;
    [SerializeField]
    private Button ExitButton, RecordButton, SaveButton, ResetButton;
    private Animator anim; 
    public static wUIController instance = null;
    public List<GameObject> disableObjects;
    private void Start()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this.gameObject);
        }
        //if(gifProcessing!=null)
         //   gifProcessing.SetActive(false);
        anim = GetComponent<Animator>();
        
        Record.OnSavedGIFEvent += OnSavedGIFEvent;
       
    }

    void OnSavedGIFEvent(SaveState saveState)
    {
        print("OnSavedGIFEvent - state = " + saveState.ToString());

        //if (gifProcessing != null)
        //    gifProcessing.SetActive(false);
        if (RecordButton != null)
            RecordButton.interactable=true;
        if (SaveButton != null)
            SaveButton.interactable = true;
        if (ResetButton != null)
            ResetButton.interactable = true;

        if (saveState == SaveState.Done)
        {

            ShareVideo();
        }
        else if (saveState == SaveState.Saving)
        {
            
        }
    }
    public void ApplicationQuit()
    {
        Application.Quit();
    }
    void updateAnimStates()
    {
        anim.SetBool("Recording", recordState);
        anim.SetBool("Sharing", shareState);
        foreach (GameObject go in disableObjects)
            go.SetActive(!shareState);
    }
    public void RecordVideo()
    {
        Debug.Log("RecordVideo()");
        recordState = true;
        shareState = false;
        updateAnimStates();
        if (recordState)
        {
            Record.DORec();
        }
        else
        {
            Record.DOReset();
        }
        
    }
    public void ResetRecord()
    {
        shareState = false;
        recordState = false;
        updateAnimStates();
        Record.DOReset();
    }
    public void SaveVideo()
    {
        recordState = false;
        shareState = true;
        updateAnimStates();
       // if (gifProcessing != null)
       //     gifProcessing.SetActive(true);
        if (RecordButton != null)
            RecordButton.interactable = false;
        if (SaveButton != null)
            SaveButton.interactable = false;
        if (ResetButton != null)
            ResetButton.interactable = false;
        Record.DOSave();
    }
    public void ShareVideo()
    {
        shareState = true;
        recordState = false;
        updateAnimStates();
        Record.DOShare(ShareType.Native);
    }

}

