﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using AppAdvisory.VSGIF;
public class wShareScript : MonoBehaviour
{

    public void wGifShare()
    {
        Debug.Log("Sharing image:" + Record.GifElement.streamingGif);
  
        new NativeShare().AddFile(Record.GifElement.streamingGif).SetSubject("Funhouse Toronto - Reflexion Experience").SetText("An underwater world at Funhouse Toronto!").Share();

        wUIController.instance.ResetRecord();
    }
}
