﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class wGenerateARCam : MonoBehaviour
{

    [MenuItem("Wack Menu/Make AR Camera", false, 0)]
    public static void MakeARCAM()
    {
        Debug.Log("Make AR CAM");
        GameObject cam = new GameObject("NEW AR CAM");
        cam.AddComponent<Camera>();
        var vuf=cam.AddComponent<Vuforia.VuforiaBehaviour>();
       
        cam.AddComponent<DefaultInitializationErrorHandler>();
        cam.AddComponent<AppAdvisory.VSGIF.Recorder>();
        cam.GetComponent<AppAdvisory.VSGIF.Recorder>().gifElement = GameObject.FindObjectOfType<AppAdvisory.VSGIF.GIFElement>();
        wCameraController c= cam.AddComponent<wCameraController>();
        GameObject hitEffect = Resources.Load<GameObject>("PetParticle1"),
                    petEffect = Resources.Load<GameObject>("PetParticle2"),
                    finalFish = Resources.Load<GameObject>("Goldfish-NetFish");

        Material mat1 = Resources.Load<Material>("GoldfishDarkest"),
                mat2 = Resources.Load<Material>("GoldfishMedium");
        c.hitEffect = hitEffect;
        c.petEffect = petEffect;
        c.prefabFinalFish=new GameObject[1] { finalFish };
        c.SkinList.Add(mat1);
        c.SkinList.Add(mat2);

    }

}
