﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public class wNetworkMessage
{
    public wNetFish netFish;
    public wAnimationEvent animEvent;
}
[System.Serializable]
public class wAnimationEvent
{
    public string ObjectName;
    public string AnimationName;
}
public class wNetSendCollider : MonoBehaviour
{
    [SerializeField]
    private GameObject exitParticle;
    public int index = 0;

    float[] color2floatarray3(Color c)
    {
        return new float[3] { c.r, c.g, c.b };
    }
    float[] vector3tofloatarray3(Vector3 v)
    {
        return new float[3] { v.x, v.y, v.z };
    }
    private void OnCollisionEnter(Collision collision)
    {
        Transform t = collision.other.transform;
        Debug.Log("OnCollisionEnter:" + t.name);
        if (t != null && t.GetComponent<wNetGoldfish>() != null)
        {
            wNetFish sendFish = t.GetComponent<wNetGoldfish>().myFish;
            sendFish.portalIndex = index;
            sendFish.scale = vector3tofloatarray3(t.localScale);
            
            t.parent = transform;
            sendFish.position = vector3tofloatarray3(t.localPosition);
            sendFish.rotation = vector3tofloatarray3(t.localEulerAngles);
            GameObject exitObject = Instantiate(exitParticle);
            exitObject.transform.position = t.position;
            
            Destroy(t.gameObject);
            Debug.Log("Received sendFish:" + sendFish.fishName);
            wNetworkMessage message = new wNetworkMessage()
            {
                netFish=sendFish,
                animEvent=null
            };
            string jsonstr = JsonUtility.ToJson(message);
            wNetworkManager.instance.SendNetworkMessage(jsonstr);
        }
        //
    }
}
