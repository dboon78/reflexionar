﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public class wNetFish
{
    public string fishName;
    public string meshName;
    public float[] position = new float[3] { 0, 0, 0 };
    public float[] rotation = new float[3] { 0, 0, 0 };
    public float[] scale = new float[3] { 1, 1, 1 };
    public int petCount = 0;
    public int colorIndex = 0;
    public float lastTrigger = 0;
    public int portalIndex;
}
public class wCameraController : MonoBehaviour
{
     int petMultiplier = 2;
    public GameObject hitEffect,petEffect;
   // [SerializeField]
   // private List<fishColorPalette> colorProgression = new List<fishColorPalette>();
    public List<Material> SkinList = new List<Material>();
    public GameObject[] prefabFinalFish;
    [HideInInspector]
    public List<wNetFish> FishList = new List<wNetFish>();
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.Mouse0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            //Vector3 dir = transform.TransformDirection(Vector3.forward);
            //Vector3 pos = transform.position;
           // Debug.DrawRay(pos, dir * 100, Color.red);

            if (Physics.Raycast(ray, out hit, Mathf.Infinity))//(Physics.Raycast(ray, out hit, 7f))
            {
                Debug.Log("Hit:" + hit.transform.name);
                if (hit.transform&&hit.transform.GetComponent<SchoolChild>())
                {
                    handleChildHit(hit);
                }
            }
        }
        
    }
    void handleChildHit(RaycastHit hit)
    {
        Transform t = hit.transform;
        bool fishFound = false;
        if (FishList.Count > 0) { 
            for (int i = 0; i < FishList.Count; i++)
            {                
                if (FishList[i].fishName == hit.transform.name)
                {
                    Debug.Log("Found Fish " + hit.transform.name);
                    fishFound = true;
                    FishList[i].fishName = hit.transform.name;
                    var sm = hit.transform.GetComponentInChildren<SkinnedMeshRenderer>();
                    if (sm != null)
                    {
                        FishList[i].meshName = sm.sharedMesh.name;
                    }
                    //FishList[i].position = vector3tofloatarray3(hit.transform.position);
                    //FishList[i].rotation = vector3tofloatarray3(hit.transform.eulerAngles);
                    PetFish(i,hit.transform);
                }
            }
        }

        if (!fishFound)
        {
            int randomNumber = (int)Random.Range(0, 10000);
            hit.transform.name = hit.transform.name + randomNumber;
            Debug.Log("Adding Fish " + hit.transform.name);
            var sm = hit.transform.GetComponentInChildren<SkinnedMeshRenderer>();
            string meshName="";
            if (sm != null)
            {
                meshName = sm.sharedMesh.name;
            }
            wNetFish newFish = new wNetFish()
            {
                fishName = hit.transform.name,
                meshName = meshName,
                //fishColor1 = color2floatarray3(colorProgression[0].fishColor1),
                //fishColor2 = color2floatarray3(colorProgression[0].fishColor2),
                // position = vector3tofloatarray3(hit.transform.localPosition),
                //rotation = vector3tofloatarray3(hit.transform.localEulerAngles)
            };
            FishList.Add(newFish);
            PetFish(FishList.Count - 1,hit.transform);
        }

        

    }



    void PetFish(int index, Transform t)
    {
        //if (Time.time > FishList[index].lastTrigger + 0.5f)
        //{
        Debug.Log("PetFish happening for " + t.name);
        FishList[index].lastTrigger = Time.time;
        FishList[index].petCount++;
        if (FishList[index].petCount % petMultiplier == 0)
        {
            //ColorProgression(index,t);
            MaterialProgression(index, t);
            var progresseffect = Instantiate(hitEffect);
            progresseffect.transform.position = t.position;
        }
        var effect = Instantiate(petEffect);
        effect.transform.position = t.position;

        //}
        //else
            //Debug.Log("")
    }
    void ProgressionComplete(int index, Transform t)
    {

        /*string jsonstr = JsonUtility.ToJson(FishList[index]);
        wNetworkManager.instance.SendNetworkMessage(jsonstr);*/
        GameObject newFish = null;
        foreach (GameObject go in prefabFinalFish) {
            var sm = go.GetComponentInChildren<SkinnedMeshRenderer>();
            if (sm!=null&&sm.sharedMesh.name == FishList[index].meshName)
            {
                newFish = Instantiate(go);
                newFish.name = FishList[index].fishName;
                break;
            }
        }
        if (newFish != null)
        {
            Debug.Log("Instantiated " + newFish.name);
            t.parent = null;
            newFish.transform.position = t.position;
            newFish.transform.rotation = t.rotation;
            newFish.transform.localScale = t.localScale;
            newFish.GetComponent<wNetGoldfish>().initFish(FishList[index]);
            Destroy(t.gameObject);
        }
    }
   /* void ColorProgression(int index, Transform t)
    {
        if (FishList[index].colorIndex > colorProgression.Count)
            return;
        FishList[index].colorIndex++;
       // FishList[index].fishColor1 = color2floatarray3(colorProgression[FishList[index].colorIndex].fishColor1);
       // FishList[index].fishColor2 = color2floatarray3(colorProgression[FishList[index].colorIndex].fishColor2);
        SkinnedMeshRenderer mr = t.GetComponentInChildren<SkinnedMeshRenderer>();
        mr.material.SetColor(0, colorProgression[FishList[index].colorIndex].fishColor1);
        mr.material.SetColor(1, colorProgression[FishList[index].colorIndex].fishColor2);
        
    }*/
    void MaterialProgression(int index, Transform t)
    {
        Debug.Log("MaterialProgression FishList["+index+"].colorIndex" + FishList[index].colorIndex);
        Debug.Log("SkinList.Count = " + SkinList.Count);
        if (FishList[index].colorIndex > SkinList.Count)
        {
            Debug.Log("Progression complete for " + t.name);
            ProgressionComplete(index, t);
            return;
        }
        Debug.Log("SchoolChild hit: " + t.name);
        FishList[index].colorIndex++;
        SkinnedMeshRenderer mr = t.GetComponentInChildren<SkinnedMeshRenderer>();
        mr.material = SkinList[FishList[index].colorIndex];
        var effect = Instantiate(hitEffect);
        effect.transform.position = t.position;
    }
}
