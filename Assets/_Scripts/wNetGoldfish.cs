﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class wNetGoldfish : MonoBehaviour
{
    [HideInInspector]
    public wNetFish myFish;
    // Start is called before the first frame update

    [HideInInspector]
    public Transform targetTransform=null;
    private Vector3 targetPos=Vector3.zero;
    private Vector3 startPos;

    private float timeStep=0;

    [HideInInspector]
    public float vectorMultiplier = 0.05f;
   public void initFish(wNetFish _fish)
    {
        myFish = _fish;
        startPos = transform.position;
        if (targetTransform == null)
        {
            float closest = 9999;
            foreach(wNetSendCollider portal in GameObject.FindObjectsOfType<wNetSendCollider>())
            {
                float distance = Vector3.Distance(transform.position, portal.transform.position);
                Debug.Log("distance:" + portal.name + "   - " + distance);
                if (distance < closest)
                {
                    closest = distance;
                    targetTransform = portal.transform;
                    Debug.Log("New targetTranform=" + targetTransform.name);
                }
            }
            //targetTransform = wNetSendCollider.instance.transform;
            targetPos = new Vector3(
                targetTransform.position.x,
                targetTransform.position.y,
                targetTransform.position.z);

        }
    }
    // Update is called once per frame
    void Update()
    {

        if (targetPos != Vector3.zero)
        {
            Vector3 targetDir = targetPos - transform.position;


            // The step size is equal to speed times frame time.
            float step = vectorMultiplier * (Time.deltaTime);

            //Vector3 newDir = Vector3.(transform.forward, targetDir, step, 0.0f);
            //Debug.DrawRay(transform.position, newDir, Color.red);
            //Debug.DrawRay(transform.position, targetDir, Color.green);
            //transform.eulerAngles = newDir;
            transform.LookAt(targetTransform);
            timeStep += Time.deltaTime;
            Vector3 newPos = Vector3.Lerp(startPos, targetPos, timeStep/3);
            transform.position = newPos;


        }
    }
}
