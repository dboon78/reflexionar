﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(SchoolController))]
public class wNetSchoolController : MonoBehaviour
{
    GameObject NetFishGameObject;
    public wNetFish netFishData;
    public Transform targetTransform = null;
    private Vector3 targetPos = Vector3.zero;
    private SchoolController controller;
    // Start is called before the first frame update
    void Start()
    {

    }

    void OnFishSpawned(SchoolChild child)
    {
        Debug.Log("child spawned:" + child.name);
        child.transform.position = new Vector3(netFishData.position[0], netFishData.position[1], netFishData.position[2]);
        child.transform.eulerAngles=new Vector3(netFishData.rotation[0], netFishData.rotation[1], netFishData.rotation[2]);
        child.transform.localScale=new Vector3(netFishData.scale[0], netFishData.scale[1], netFishData.scale[2]);
    }
    private void Update()
    {
        if (controller == null)
        {
            controller = GetComponent<SchoolController>();
            if(controller!=null)
            { 
                controller.OnRoamerAdded.AddListener(OnFishSpawned);
            }
        }
        /*if (targetTransform == null)
        {
            targetTransform = wNetSendCollider.instance.transform;
            targetPos = new Vector3(
                Random.Range(targetTransform.position.x - .2f, targetTransform.position.x + .2f),
                Random.Range(targetTransform.position.y - .2f, targetTransform.position.y + .2f),
                targetTransform.position.z);

            transform.position = targetPos;

        }*/
    }
}
